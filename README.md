# Installation

- Add the follow code on the end of your bootstrap:
```php
/**
 * PHPUtils Component load
 */
App::build(array(
    'Controller/Component' => array(
        APP . 'Controller'.DS.'Component'.DS.'PHPUtils' . DS,
    )
), App::APPEND);
```
- Open terminal on root document of your project and run the following command:
```shell
git submodule add https://gabrieldissotti@bitbucket.org/gabrieldissotti/php-utils.git app/Controller/Component/PHPUtils
```

# How to use

- Add the component on your controller:

```php
	public $components = ['Format'];
```

- Follow example:

```php
echo $this->Format->onlyNumbers('abcd123'); // Print only number of the string
```


